from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()

    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            name = form.save(False)
            name.save()
            return redirect("todo_list_detail", id=name.id)
    else:
        form = TodoForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoForm(instance=todo)
    context = {
        "todo_object": todo,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = ItemForm()

    context = {"form": form}
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = ItemForm(instance=todo)
    context = {
        "todo_object": todo,
        "form": form,
    }
    return render(request, "todos/update.html", context)
